# Using GitLab for CI/CD on Docker Swarm

This repository is used as a demonstration on How we can leverage GitLab to 
automatically deploy an application on a docker Swarm cluster for the article 
in the [GitLab Documentation](https://docs.gitlab.com/ce/ci/examples/docker_swarm_with_gitlab_ci_cd/index.html)

[![pipeline status](https://gitlab.com/allamand/example-voting-app/badges/master/pipeline.svg)](https://gitlab.com/allamand/example-voting-app/commits/master)

The pipeline needs a valid play-with-docker url session stored in the `PWD_URL` gitlab variable (see tutorial for further info)

<!--> Since Play With Docker recently enabled Docker account authentication, it is not possible anymore at this time of writing to uses docker-machine PWD driver to set-up the nodes on PWD. (check this [Issue](https://github.com/play-with-docker/docker-machine-driver-pwd/issues/12)) -->

> We are going to uses the SSH button and to set a DOCKER_HOST var to enable communication with our docker nodes on PWD

<!--You can find the associated tutorial in the [GitLab Page](https://allamand.gitlab.io/example-voting-app/) -->

## What

Since we deploy our application on a [Docker Swarm
Mode](https://docs.docker.com/engine/swarm/) cluster, we will make use of
[Docker overlay
network](https://docs.docker.com/engine/userguide/networking/get-started-overlay/#overlay-networking$
and create a `public` and `private` network. Our application will have 2
frontend services that must be exposes on the Internet, so we set-up a double
network attachment on public and private network. The backends services will not
be exposed on Internet and will only be attached on the private network. The
[Traefik](https://traefik.io/) Reverse Proxy role is to dispatch our requests
from internet to the frontend services and will be listening only on the public
Network. Only Traefik will exposes ports on the swarm.

This microservices application can be describe as :

- A Docker Swarm **public network** name `traefik_net` to expose our frontend
  services :
    - a **Traefik** reverse Proxy which is the public endpoint for the 2
      frontend services:
    - a **vote** **Python** web-app, which lets you vote between two options
    - a **result** **Node.js** web-app which shows the results of the voting in
      real-time.
- A Docker Swarm **private network** name `private` which links our frontend to
  our backend micro services :
    - a **Redis** queue which collects new votes
    - a **worker** **.NET** service which consumes votes, and store them in..
    - a **Postgres** database, backed by a Docker Volume

![Application architecture](docs/img/gitlab_vote_traefik_archi.png)

## Crédit

This is a fork from the original [docker voting example
application](https://github.com/dockersamples/example-voting-app) on which I
added :
- a `traefik.yml` docker stack file to create the traefik stack and docker
  public network
- a `docker-compose-gitlab.yml` file to build and push each microservice app in
  the gitlab docker registry
- a `docker-stack-traefik.yml` stack file to deploy tha application as a docker
  stack
- a `.gitlab-ci.yml` file to enable our Gitlab Pipeline
- I updated the `vote` and `result` services to work with path /vote and /result
  since on Play-With-Docker we can only do Path-based http routing.

